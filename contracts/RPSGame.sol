//SPDX-License-Identifier: license
pragma solidity ^0.8.11;

contract RPSGame {
    bytes32 private constant COMMITMENT_SALT =
        bytes32("0x6a377e3f514827685962");

    struct Player {
        address player;
        bytes32 choice;
        uint256 timestamps;
    }

    // Player public player1;
    // Player public player2;

    // struct Player2 {
    //     address player;
    //     bytes32 choice;
    //     uint timestamps;
    // }
    // enum moves {
    //   keccak256(abi.encodePacked("ROCK")),
    //   keccak256(abi.encodePacked("SCISSORS")),
    //   keccak256(abi.encodePacked("PAPER"))
    // }
    //   mapping(address => Player) ;
    bytes32 public constant ROCK = keccak256(abi.encodePacked("ROCK"));
    bytes32 public constant SCISSORS = keccak256(abi.encodePacked("SCISSORS"));
    bytes32 public constant PAPER = keccak256(abi.encodePacked("PAPER"));

    mapping(address => Player) players;
    function play(bytes32 _choice) external {
        Player memory _player;
        _player.player = msg.sender;
        _player.choice = _choice;
        _player.timestamps = block.timestamp;
        players[msg.sender] = _player;
    }

    function createCommitment(address _player, uint8 _rand)
        private
        pure
        returns (bytes32)
    {
        return keccak256(abi.encodePacked(_player, _rand));
    }

    function play(int name) public {
      
    }

    // function getHash() public view returns (bytes32) {
    //     return keccak256(abi.encodePacked(block.number, block.difficulty));
    // }
}
