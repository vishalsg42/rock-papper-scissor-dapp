//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.11;

import "hardhat/console.sol";

contract RPSGame {
  bytes32  constant private COMMITMENT_SALT = bytes32("0x6a377e3f514827685962");

  struct Player1 {
    address player;
    bytes32 choice;
    uint timestamps;
  }

  struct Player2 {
    address player;
    bytes32 choice;
    uint timestamps;
  }

  // enum moves {
  //   keccak256(abi.encodePacked("ROCK")),
  //   keccak256(abi.encodePacked("SCISSORS")),
  //   keccak256(abi.encodePacked("PAPER"))
  // }

  mapping(bytes32 => bytes32) public moves;
  // moves[bytes32("ROCK")] =  keccak256(abi.encodePacked("ROCK"));
  // moves[bytes32("SCISSORS")] =  keccak256(abi.encodePacked("SCISSORS"));
  // moves[bytes32("PAPER")] =  keccak256(abi.encodePacked("PAPER"));

  // struct PlayerPool {
  //   Player player1;
  //   Player player2;
  //   uint timestamps;
  //   // uint pricePool;
  // }

  // mapping(uint => PlayerPool) public gamePool;

  // constructor() {

  // }
  function createCommitment(bytes32 choice) private view returns(bytes32) {
    return keccak256(abi.encodePacked(msg.sender,choice,COMMITMENT_SALT));
    // 
  }
  // function joinGame() public {

  // }

  // mappings(address => Player) ;
}